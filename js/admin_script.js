function visible(elem){
    document.getElementById("control").style.display="none"
    document.getElementById(elem).style.display="block";
}

function invisible(elem){
    document.getElementById(elem).style.display="none";
    document.getElementById("control").style.display="block"
}

function retourner(){
    document.getElementById("modifier2").style.display="none"
    document.getElementById("control").style.display="block"
}

function liste_films(){
    document.getElementById("pageControl").style.display="none";
        var tabFilm=app.films.getElementsByTagName("film");
        document.getElementById("listeFilms").innerHTML+= "\
            <div class='container'>\
            <div class='row text-center mt-5' id='liste'>";
        for (var i=0; i < tabFilm.length; i++){
            var numero = tabFilm[i].getElementsByTagName("numero")[0].firstChild.nodeValue;
            var titre = tabFilm[i].getElementsByTagName("titre")[0].firstChild.nodeValue;
            var realisateur = tabFilm[i].getElementsByTagName("realisateur")[0].firstChild.nodeValue;
            var categorie = tabFilm[i].getElementsByTagName("categorie")[0].firstChild.nodeValue;
            var duree = tabFilm[i].getElementsByTagName("duree")[0].firstChild.nodeValue;
            var prix = tabFilm[i].getElementsByTagName("prix")[0].firstChild.nodeValue;
            var pochette = tabFilm[i].getElementsByTagName("pochette")[0].firstChild.nodeValue;
            var preview = tabFilm[i].getElementsByTagName("preview")[0].firstChild.nodeValue;
            document.getElementById("liste").innerHTML+= "\
            <div class='col-3 mb-3' id='num"+numero+"'>\
                    <div class='card h-100'>\
                        <img  class='card-img-top img' id='"+numero+"' src='"+pochette+"' alt='' onclick='showModal(this);' data-toggle='modal' data-target='#exampleModalCenter' data-id='"+numero+"'>\
                        <div class='card-body'>\
                            <h4 class='card-title titre'>"+titre+"</h4>\
                            <p class='hidden preview'>"+preview+"</p>\
                            <p class='card-text rea'>"+realisateur+"</p>\
                            <p class='card-text categ'>"+categorie+"</p>\
                            <p class='card-text prix'>"+prix+"$</p>\
                        </div>\
                    </div>\
            </div>"    
        }
        document.getElementById("listeFilms").innerHTML+= "</div></div>";      
}

function controlPannel(){
    document.getElementById("listeFilms").innerHTML="";
    document.getElementById("pageControl").style.display="block";
}

//acquisition des valeurs du formulaire enregistrer + REGEXP
if(document.getElementById("EnregistrerForm") != undefined){
    document.getElementById("EnregistrerForm").addEventListener('submit', function(event){
        event.preventDefault();
        //Récuperer les informations du formulaire et construir l'objet nouveau_film
        var enrForm = document.querySelector("#EnregistrerForm");
        nouveau_numero = enrForm.querySelector(".numero").value;
        nouveau_titre = enrForm.querySelector(".titre").value;
        nouveau_rea = enrForm.querySelector(".realisateur").value;
        nouveau_categ = enrForm.querySelector(".categorie").value;
        nouveau_duree = enrForm.querySelector(".duree").value;
        nouveau_prix = Number(enrForm.querySelector(".prix").value);
        nouveau_pochette = enrForm.querySelector(".pochette").value;
        nouveau_preview = enrForm.querySelector(".preview").value;
        var tabFilm = app.films.getElementsByTagName("film");
        var exist = 0;
        for(i=0; i<tabFilm.length; i++){
            var numero = tabFilm[i].getElementsByTagName("numero")[0].firstChild.nodeValue;
            if(nouveau_numero == numero){
                exist = 1;
            }
        }
        if(nouveau_numero==""|| nouveau_titre==""|| nouveau_rea==""|| nouveau_categ==""|| nouveau_duree==""|| nouveau_prix==""|| nouveau_pochette==""|| nouveau_preview==""){
            alert("veuillez remplir tous les champs");
        }
        else if(exist == 1) {
            alert("ce numero de film existe déjà. choisissez un autre numero");
        } 
        else{
            enregistrer_film(nouveau_numero, nouveau_titre, nouveau_rea, nouveau_categ, nouveau_duree, nouveau_prix, nouveau_pochette, nouveau_preview);
        }
       
    })
}

//acuisition des valeurs du formulaire modifier, REGEXP, puis rediriger vers formulaire de modification du film
if(document.getElementById("modifier") != undefined){
    document.getElementById("modifierForm").addEventListener('submit', function(event){
        event.preventDefault();
        var modifierForm = document.querySelector("#modifierForm");
        filmNumero = modifierForm.querySelector(".numero").value;
        var exist = 0;
        var tab_i = "x";
        var tabFilm = app.films.getElementsByTagName("film");
        for(var i=0; i<tabFilm.length; i++){
            var numero = tabFilm[i].getElementsByTagName("numero")[0].firstChild.nodeValue;
            if(filmNumero == numero){
                var exist = 1;
                tab_i = tabFilm[i]
            }
        }
        if(filmNumero==""){
            alert("veuillez remplir le formulaire");
        }
        else if(exist == 0){
            alert("le film numero "+filmNumero+" n'existe pas")
        }
        else if(exist == 1){
            document.getElementById("modifier").style.display="none";
            document.getElementById("modifier2").style.display="block";
            var form2 = document.querySelector("#formulaire_modifier");
            form2.querySelector(".numero").value= filmNumero;
            form2.querySelector(".titre").value = tab_i.getElementsByTagName("titre")[0].firstChild.nodeValue;
            form2.querySelector(".realisateur").value = tab_i.getElementsByTagName("realisateur")[0].firstChild.nodeValue;
            var select = form2.querySelector(".categorie")
            select.options[select.selectedIndex].value = tab_i.getElementsByTagName("categorie")[0].firstChild.nodeValue;
            form2.querySelector(".duree").value = tab_i.getElementsByTagName("duree")[0].firstChild.nodeValue;
            form2.querySelector(".prix").value = tab_i.getElementsByTagName("prix")[0].firstChild.nodeValue;
            form2.querySelector(".nomPochette").value = tab_i.getElementsByTagName("pochette")[0].firstChild.nodeValue;
            form2.querySelector(".preview").value = tab_i.getElementsByTagName("preview")[0].firstChild.nodeValue;
        } 
    })
}

if(document.getElementById("modifier2") != undefined){
    document.getElementById("formulaire_modifier").addEventListener('submit', function(event){
        event.preventDefault();
        var numero = document.getElementById("numero_modif").value;
        var titre = document.getElementById("titre_modifier").value;
        var rea = document.getElementById("rea_modifier").value;
        var categ = document.getElementById("categ_modifier").value;
        var duree = document.getElementById("duree_modifier").value;
        var prix = document.getElementById("prix_modifier").value;
        var image = document.getElementById("img_modifier").value;
        var preview = document.getElementById("preview_modifier").value;
        alert("le film numero"+numero+"à été bien enregistrer");
        liste_films()
        var div = document.getElementById("num"+numero);
        div.querySelector(".img").src = "/~azarouaa/IFT1142/TP3/"+image;
        div.querySelector(".titre").innerHTML = titre;
        div.querySelector(".rea").innerHTML = rea;
        div.querySelector(".categ").innerHTML = categ
        div.querySelector(".prix").innerHTML = prix+"$";
    })
}

//retirer un film
if(document.getElementById("retirer") != undefined){
    document.getElementById("retirerForm").addEventListener('submit', function(event){
    event.preventDefault();
    var enleverForm = document.querySelector("#retirerForm");
    numero = enleverForm.querySelector("#numero_retirer").value;
    var tabFilm = app.films.getElementsByTagName("film");
        var exist = 0;
        for(i=0; i<tabFilm.length; i++){
            var numero_xml = tabFilm[i].getElementsByTagName("numero")[0].firstChild.nodeValue;
            if(numero == numero_xml){
                exist = 1;
            }
        }
        if(numero==""){
            alert("veuillez remplir tous les champs");
        }
        else if(exist != 1) {
            alert("ce numero de film n'existe pas. choisissez un autre numero");
        } 
        else if(exist ==1){
            alert("le film numero"+numero+" a été supprimé");
            liste_films()
            var div = document.getElementById("num"+numero);
            div.parentNode.removeChild(div);
        }
    })
}

//requete ajax pour enregistrer, modifier et retirer un film
function enregistrer_film(el1, el2, el3, el4, el5, el6, el7, el8){
    liste_films()
            document.getElementById("liste").innerHTML+= "\
            <div class='col-3 mb-3' id="+el1+">\
                    <div class='card h-100'>\
                        <img  class='card-img-top' id='"+el1+"' src='"+el7+"' alt='' onclick='showModal(this);' data-toggle='modal' data-target='#exampleModalCenter' data-id='"+numero+"'>\
                        <div class='card-body'>\
                            <h4 class='card-title'>"+el2+"</h4>\
                            <p class='hidden'>"+el8+"</p>\
                            <p class='card-text'>"+el3+"</p>\
                            <p class='card-text'>"+el4+"</p>\
                            <p class='card-text'>"+el6+"$</p>\
                        </div>\
                    </div>\
            </div>" 
            
}

