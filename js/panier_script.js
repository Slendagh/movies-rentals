//creation du panier en utilisant les fonctions du DOM
function showPanier(){
    rendreVisible('acceuil_membre', 'panier')
    var table = document.createElement("table");
    table.setAttribute("class", "table");
    var div = document.getElementById("tableIci");

    if(div.hasChildNodes){
        div.removeChild(div.childNodes[0]);
    }
    
    div.appendChild(table);

    function generateTableHead(table) {
        let thead = table.createTHead();
        let row = thead.insertRow();

        let th_pochette = document.createElement("th");
        let pochette = document.createTextNode("pochette");
        th_pochette.appendChild(pochette);
        row.appendChild(th_pochette);

        let th_titre = document.createElement("th");
        let titre = document.createTextNode("titre");
        th_titre.appendChild(titre);
        row.appendChild(th_titre);

        let th_quantite = document.createElement("th");
        let quantite = document.createTextNode("quantite");
        th_quantite.appendChild(quantite);
        row.appendChild(th_quantite);

        let th_prix = document.createElement("th");
        let prix = document.createTextNode("prix");
        th_prix.appendChild(prix);
        row.appendChild(th_prix);
    }

    function generateTable(table, data) {
        for(var i=0; i<data.length; i++){
            var row = table.insertRow();
            //pochette cell
            var cellP = row.insertCell();
            var img = document.createElement("img");
            img.setAttribute("src", data[i]["pochette"]);
            img.setAttribute("id", "petiteImg");
            var contentP = document.createTextNode("");
            img.appendChild(contentP);
            cellP.appendChild(img);
            //titre cell
            var cellT = row.insertCell();
            var contentT = document.createTextNode(data[i]["titre"]);
            cellT.appendChild(contentT);
            //quantite cell
            var cellQ = row.insertCell();
            var contentQ = document.createTextNode(data[i]["quantite"]);
            cellQ.appendChild(contentQ);
            //prix cell
            var cellPr = row.insertCell();
            var contentPr = document.createTextNode(eval(data[i]["prix"]*data[i]["quantite"])+"$");
            cellPr.appendChild(contentPr);
            //suppr cell
            var cellS = row.insertCell();
            var button = document.createElement("button");
            button.setAttribute("class", "btn btn-sm btn-danger supprimer_panier");
            button.setAttribute("data-numero", i)
            var baliseI = document.createElement("i");
            baliseI.setAttribute("class", "fa fa-trash");
            button.appendChild(baliseI);
            var contentS = document.createTextNode(data[i][""]);
            button.appendChild(contentS);
            cellS.appendChild(button);

        }
    }
    
    function generatePrix(){
        var sous_total = document.getElementById("sous_total").innerHTML= 0.00;
        var tvq = document.getElementById("tvq").innerHTML= 0.00;
        var tps = document.getElementById("tps").innerHTML= 0.00;
        var total = document.getElementById("total").innerHTML= 0.00;
        for(var i=0; i<tabLocation.length; i++){
            sous_total = Number(sous_total);
            sous_total += tabLocation[i]["prix"] * Number(tabLocation[i]["quantite"]);
            document.getElementById("sous_total").innerHTML= sous_total.toFixed(2);
            
            tvq = Number(tvq);
            tvq = (0.0997*sous_total);
            document.getElementById("tvq").innerHTML= tvq.toFixed(2);

            tps = Number(tps);
            tps = (0.05*sous_total);
            document.getElementById("tps").innerHTML= tps.toFixed(2);

            total = Number(total);
            total = sous_total + tps + tvq;
            document.getElementById("total").innerHTML = total.toFixed(2);

        }
    }
    generateTableHead(table);
    generateTable(table, tabLocation);
    generatePrix();
}

//ajouter films dans localStorage de location
function ajouterPanier(element){
    var tabFilm=app.films.getElementsByTagName("film");
    var numeroChoisi = element.dataset.numero;
    var option_value = element.previousElementSibling.firstChild.value;
    for(var i=0; i<tabFilm.length; i++){
        var numero = tabFilm[i].getElementsByTagName("numero")[0].firstChild.nodeValue;
        var pochette = tabFilm[i].getElementsByTagName("pochette")[0].firstChild.nodeValue;
        var titre = tabFilm[i].getElementsByTagName("titre")[0].firstChild.nodeValue;
        var prix = tabFilm[i].getElementsByTagName("prix")[0].firstChild.nodeValue;
        if(numeroChoisi == numero){
            objLocation={"pochette": pochette, "titre":titre, "quantite":option_value, "prix": prix, "":"supprimer"}
            tabLocation.push(objLocation);
            //stringify
            let objLocation_serialized = JSON.stringify(objLocation);
            //put in local storage as string
            localStorage.setItem("objLocation", objLocation_serialized);
        }
    }
}

//supprimer element du panier
if(document.getElementById("tableIci") !== null){
    document.getElementById("tableIci").addEventListener("click", function(event){
        if(event.target.classList.contains("supprimer_panier")){
            var numero = event.target.dataset.numero
            delete tabLocation[numero];
            tabLocation.splice(numero, 1)
            localStorage.removeItem("objLocation");
            showPanier()
        }   
    })
}

//alert du payement
function payer(){
    alert("vous avez payer");
}