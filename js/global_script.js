function rendreVisible(elem1, elem2){
    document.getElementById("load_here").innerHTML="";
    document.getElementById(elem1).style.display="none"
    document.getElementById(elem2).style.display="block";
}

//parse xml via ajax
app={};
app.films = null;
var etat;
function chargerXML(){
	$.ajax({
		url:"../donnees/films.xml",
        type:"GET",
		dataType:"xml",
        success: function(reponse){
            app.films = reponse;
            loadTabFilm(reponse);
            acceuil_Membre(reponse);
        }, 
        async: true,
        error: function(error){
            console.log(error);
        }
    });  
}

var titre;
var numero;
var tabFilm=null;
//load films dans acceuil
function loadTabFilm(films){
    if(document.getElementById("load_here") != undefined){
        var tabFilm=films.getElementsByTagName("film");
        document.getElementById("load_here").innerHTML+= "\
            <div class='container'>\
            <div class='row text-center mt-5' id='loadTabFilm_Acceuil'>";
        for (var i=0; i < tabFilm.length; i++){
            var numero = tabFilm[i].getElementsByTagName("numero")[0].firstChild.nodeValue;
            var titre = tabFilm[i].getElementsByTagName("titre")[0].firstChild.nodeValue;
            var realisateur = tabFilm[i].getElementsByTagName("realisateur")[0].firstChild.nodeValue;
            var categorie = tabFilm[i].getElementsByTagName("categorie")[0].firstChild.nodeValue;
            var duree = tabFilm[i].getElementsByTagName("duree")[0].firstChild.nodeValue;
            var prix = tabFilm[i].getElementsByTagName("prix")[0].firstChild.nodeValue;
            var pochette = tabFilm[i].getElementsByTagName("pochette")[0].firstChild.nodeValue;
            var preview = tabFilm[i].getElementsByTagName("preview")[0].firstChild.nodeValue;
            document.getElementById("loadTabFilm_Acceuil").innerHTML+= "\
            <div class='col-3 mb-3'>\
                    <div class='card h-100'>\
                        <img  class='card-img-top' id='"+numero+"' src='"+pochette+"' alt='' onclick='showModal(this);' data-toggle='modal' data-target='#exampleModalCenter' data-id='"+numero+"'>\
                        <div class='card-body'>\
                            <h4 class='card-title'>"+titre+"</h4>\
                            <p class='hidden'>"+preview+"</p>\
                            <p class='card-text'>"+realisateur+"</p>\
                            <p class='card-text'>"+categorie+"</p>\
                            <p class='card-text'>"+prix+"$</p>\
                        </div>\
                    </div>\
            </div>"    
        }
        document.getElementById("load_here").innerHTML+= "</div></div>";      
    }
}


//categories dans nav bar
function categorie(elem){

    if(document.getElementById("load_here") != undefined){
        var div_loadTabFilm_Acceuil = document.getElementById("load_here");
        if(div_loadTabFilm_Acceuil.hasChildNodes){
            div_loadTabFilm_Acceuil.innerHTML="";
        }  
        document.getElementById("load_here").innerHTML+= "\
        <div class='container'>\
        <div class='row text-center mt-5' id='loadTabFilm_Categorie'>";   
        var tabFilm=app.films.getElementsByTagName("film");
        for(i=0; i<tabFilm.length; i++){
            var categorie = tabFilm[i].getElementsByTagName("categorie")[0].firstChild.nodeValue;
            var numero = tabFilm[i].getElementsByTagName("numero")[0].firstChild.nodeValue;
            var titre = tabFilm[i].getElementsByTagName("titre")[0].firstChild.nodeValue;
            var realisateur = tabFilm[i].getElementsByTagName("realisateur")[0].firstChild.nodeValue;
            var prix = tabFilm[i].getElementsByTagName("prix")[0].firstChild.nodeValue;
            var pochette = tabFilm[i].getElementsByTagName("pochette")[0].firstChild.nodeValue;
            var preview = tabFilm[i].getElementsByTagName("preview")[0].firstChild.nodeValue;
            if(categorie == elem){
                document.getElementById("loadTabFilm_Categorie").innerHTML+= "\
                <div class='col-3 mb-3'>\
                        <div class='card h-100'>\
                            <img  class='card-img-top' id='"+numero+"' src='"+pochette+"' alt='' onclick='showModal(this);' data-toggle='modal' data-target='#exampleModalCenter' data-id='"+numero+"'>\
                            <div class='card-body'>\
                                <h4 class='card-title'>"+titre+"</h4>\
                                <p class='hidden'>"+preview+"</p>\
                                <p class='card-text'>"+realisateur+"</p>\
                                <p class='card-text'>"+categorie+"</p>\
                                <p class='card-text'>"+prix+"$</p>\
                                <p class='card-text'id='insert_membre'><p>\
                            </div>\
                        </div>\
                </div>"  
            }    
        }
        document.getElementById("load_here").innerHTML+= "</div></div>";
        document.getElementById("connexion").style.display="none";
        document.getElementById("membre").style.display="none";
    }
}

//modal
function showModal(element){
    var titre = element.nextElementSibling.firstElementChild.innerHTML;
    var preview = element.nextElementSibling.firstElementChild.nextElementSibling.innerHTML;
    document.getElementById("exampleModalLongTitle").innerHTML = titre;
    document.getElementById("iframe").src = preview;   
}



//json pour utilisation de localStorage
// tableau membres
var tabMembre = [];
//tableau connexion
var tabConnexion = [{"email":"admin@gmail.com", "mot_de_passe":"admin@gmail.com"},{"email":"membre@gmail.com", "mot_de_passe":"membre@gmail.com"}];
//tableau Panier
var tabLocation = [];

//synchronisation localStorage
function synchronisation(){
    if(localStorage.getItem("objMem") != null){
        var objMemString = localStorage.getItem("objMem");
        var objMem = JSON.parse(objMemString);
        tabMembre.push(objMem);
    }

    if(localStorage.getItem("objCon") != null){
        var objConString = localStorage.getItem("objCon");
        var objCon = JSON.parse(objConString);
        tabConnexion.push(objCon);
    }
    
    if(localStorage.getItem("objLocation") != null){
        var objLocationString = localStorage.getItem("objLocation");
        var objLocation = JSON.parse(objLocationString);
        tabLocation.push(objLocation);
    }
}