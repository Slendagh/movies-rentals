
//validation REGEXP du formulaire devenir membre
function valider(regexpMdp,mdp1,mdp2,prenom,nom,email,dateN,sexe){
    if(prenom=="" || nom=="" || email=="" || dateN=="" || mdp1 =="" || sexe==""){
        alert("Vous devez remplir le formulaire...")
        return false;
    }
    if(!regexpMdp.test(mdp1)){
        alert("le mot de passe doit contenir au moins 6 caractères ( lettres/chiffres)");
        return false;
    }
    if(mdp1 != mdp2){
        alert("les mots de passe ne sont pas identiques !");
        return false;
    }

    return true;
}
//si formulaire membre valider, remplir tableaux JSON et localStorage membre et connexion
if(document.getElementById("formMem") != undefined){
    document.getElementById("formMem").addEventListener('submit', function(event){
        event.preventDefault();
        var regexpMdp = /(\w+[\@\_\-]?){6,}/gi;
        var formMem = document.querySelector("#formMem");
        var mdp1 = formMem.querySelector(".mdp1").value;
        var mdp2 = formMem.querySelector(".mdp2").value;
        var prenom = formMem.querySelector(".prenom").value;
        var nom = formMem.querySelector(".nom").value;
        var email = formMem.querySelector(".email").value;
        var dateN = formMem.querySelector(".dateN").value;
        var sexe = document.form_Membre.sexe.value;
        if(valider(regexpMdp,mdp1,mdp2,prenom,nom,email,dateN,sexe)){
            objMem = {"nom":nom, "prenom":prenom, "date de naissance":dateN, "sexe":sexe}
            tabMembre.push(objMem);
            //stringify
            let objMem_serialized = JSON.stringify(objMem);
            //put in local storage as string
            localStorage.setItem("objMem", objMem_serialized);
            objCon = {"email":email, "mot_de_passe": mdp1};
            //stringify
            let objCon_serialized = JSON.stringify(objCon);
            //put in local storage as string
            localStorage.setItem("objCon", objCon_serialized);
            //destringify object inside localstorage
            tabConnexion.push(objCon);
            rendreVisible('membre','connexion');
        }  
    })
}

// validation REGEXP du formulaire connexion 
function validerCon(){
    var formCon = document.querySelector("#connexion");
    var mdp = formCon.querySelector(".mdpCon").value;
    var email = formCon.querySelector(".emailCon").value;
    if(email=="" || mdp==""){
        alert("vous devez remplir le formulaire...");
        return false;
    }
    else{
        for(var k=0; k<tabConnexion.length; k++){
            if(tabConnexion[k]["email"] == email && tabConnexion[k]["mot_de_passe"]== mdp){
                return true;
            }
            else{
                var count = 1;  
            }
        }
        if(count==1){
            alert("Vous n'êtes pas membre." );
            return false
        } 
    }   
}

// si formulaire membre valider, redirection vers page acceuil pour les membres
if(document.getElementById("connexion") != undefined){
    document.getElementById("connexion").addEventListener('submit', function(event){
        event.preventDefault();
        if(validerCon()){
            var formCon = document.querySelector("#connexion");
            var mdp = formCon.querySelector(".mdpCon").value;
            var email = formCon.querySelector(".emailCon").value;
            if(email == "admin@gmail.com" && mdp=="admin@gmail.com"){
                document.getElementById("page_membre").style.display="none";
                document.getElementById("page_acceuil").style.display="none";
                document.getElementById("page_admin").style.display="block";
                document.getElementById("control").style.display="block"
            }
            else{
                //document.getElementById("connexion").style.display="none";
                document.getElementById("page_acceuil").style.display="none";
                var pageMembre= document.getElementById("page_membre");
                pageMembre.style.display="block";
                document.getElementById("acceuil_membre").style.display="block";
                document.getElementById("email_ici").innerHTML = email;
            }
        }
    })
}

function acceuil_Membre(films){
    if(document.getElementById("acceuil_membre") != undefined){
        var tabFilm=films.getElementsByTagName("film");
        document.getElementById("acceuil_membre").innerHTML+= "\
            <div class='container'>\
            <div class='row text-center mt-5' id='loadTabFilm_membre'>";
        for (var i=0; i < tabFilm.length; i++){
            var numero = tabFilm[i].getElementsByTagName("numero")[0].firstChild.nodeValue;
            var titre = tabFilm[i].getElementsByTagName("titre")[0].firstChild.nodeValue;
            var realisateur = tabFilm[i].getElementsByTagName("realisateur")[0].firstChild.nodeValue;
            var categorie = tabFilm[i].getElementsByTagName("categorie")[0].firstChild.nodeValue;
            var duree = tabFilm[i].getElementsByTagName("duree")[0].firstChild.nodeValue;
            var prix = tabFilm[i].getElementsByTagName("prix")[0].firstChild.nodeValue;
            var pochette = tabFilm[i].getElementsByTagName("pochette")[0].firstChild.nodeValue;
            var preview = tabFilm[i].getElementsByTagName("preview")[0].firstChild.nodeValue;
            document.getElementById("loadTabFilm_membre").innerHTML+= "\
            <div class='col-3 mb-3'>\
                    <div class='card h-100'>\
                        <img  class='card-img-top' id='"+numero+"' src='"+pochette+"' alt='' onclick='showModal(this);' data-toggle='modal' data-target='#exampleModalCenter' data-id='"+numero+"'>\
                        <div class='card-body'>\
                        <h4 class='card-title'>"+titre+"</h4>\
                        <p class='hidden'>"+preview+"</p>\
                        <p class='card-text'>"+realisateur+"</p>\
                        <p class='card-text'>"+categorie+"</p>\
                        <p class='card-text'>"+prix+"$</p>\
                        <p class='card-text'><select class='quantite' data-num='"+numero+"'><option value='1' selected>1</option>\
                        <option value='2'>2</option>\
                        <option value='3'>3</option>\
                        <option value='4'>4</option></select></p>\
                        <a class='btn btn-primary' onClick='ajouterPanier(this);' data-numero='"+numero+"'>Ajouter</a>\
                        </div>\
                    </div>\
            </div>"    
        }
        document.getElementById("acceuil_membre").innerHTML+= "</div></div>";      
    }
}

function categorie_mem(elem){
    if(document.getElementById("acceuil_membre") != undefined){
        var div_loadTabFilm_membre = document.getElementById("acceuil_membre");
        if(div_loadTabFilm_membre.hasChildNodes){
            div_loadTabFilm_membre.innerHTML="";
        }   
        document.getElementById("panier").style.display="none";
        document.getElementById("acceuil_membre").style.display="block";

        document.getElementById("acceuil_membre").innerHTML+= "\
        <div class='container'>\
        <div class='row text-center mt-5' id='loadTabFilm_Categorie_mem'>";   
        var tabFilm=app.films.getElementsByTagName("film");
        for(i=0; i<tabFilm.length; i++){
            var categorie = tabFilm[i].getElementsByTagName("categorie")[0].firstChild.nodeValue;
            var numero = tabFilm[i].getElementsByTagName("numero")[0].firstChild.nodeValue;
            var titre = tabFilm[i].getElementsByTagName("titre")[0].firstChild.nodeValue;
            var realisateur = tabFilm[i].getElementsByTagName("realisateur")[0].firstChild.nodeValue;
            var prix = tabFilm[i].getElementsByTagName("prix")[0].firstChild.nodeValue;
            var pochette = tabFilm[i].getElementsByTagName("pochette")[0].firstChild.nodeValue;
            var preview = tabFilm[i].getElementsByTagName("preview")[0].firstChild.nodeValue;
            if(categorie == elem){
                document.getElementById("loadTabFilm_Categorie_mem").innerHTML+= "\
                <div class='col-3 mb-3'>\
                        <div class='card h-100'>\
                            <img  class='card-img-top' id='"+numero+"' src='"+pochette+"' alt='' onclick='showModal(this);' data-toggle='modal' data-target='#exampleModalCenter' data-id='"+numero+"'>\
                            <div class='card-body'>\
                                <h4 class='card-title'>"+titre+"</h4>\
                                <p class='hidden'>"+preview+"</p>\
                                <p class='card-text'>"+realisateur+"</p>\
                                <p class='card-text'>"+categorie+"</p>\
                                <p class='card-text'>"+prix+"$</p>\
                                <p class='card-text'id='insert_membre'><select class='quantite' data-num='"+numero+"'><option value='1' selected>1</option>\
                                <option value='2'>2</option>\
                                <option value='3'>3</option>\
                                <option value='4'>4</option></select></p>\
                                <a class='btn btn-primary' onClick='ajouterPanier(this);' data-numero='"+numero+"'>Ajouter</a>\
                            </div>\
                        </div>\
                </div>"  
            }    
        }
        document.getElementById("acceuil_membre").innerHTML+= "</div></div>";   
    }
}

function show_acceuil_mem(){
    document.getElementById("panier").style.display="none";
    document.getElementById("acceuil_membre").style.display="block"
    document.getElementById("acceuil_membre").innerHTML="";
    acceuil_Membre(app.films)
}